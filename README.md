# koronapaniikki.fi

![screenshot](./dist/share_picture.png "Koronatilanne")

Palvelu näyttä koronaviirustilanteen maakunnittain.
[https://koronapaniikki.fi](https://koronapaniikki.fi/)

# Devaus

```
npm install
npm start
```

## Deploy
```
npm ci
npm run-script build
firebase deploy
```