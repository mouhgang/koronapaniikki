const createGraph = require("./vis").createGraph;
const randomPointsOnPolygon = require('random-points-on-polygon');
const turf = require('turf');
const CountUp = require('countup.js').CountUp;


mapboxgl.accessToken = 'pk.eyJ1IjoicGFsaWtrIiwiYSI6ImNrNzljb2NnaTBueDIzZm55eXJpcjh0M2gifQ.DvQulSOQQxy2CpDWdytTww';
let map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/palikk/ck7nrhxsx1gxa1ipge1lkqc8r',
  center: [26.1305, 65.4376],
  maxZoom: 7,
  minZoom: 4,
  zoom: 4.5
});

map.on('load', loadGeojsons);
function loadGeojsons() {
  fetch(require("./maakunnat-simplified.geojson")).then(res => res.json())
    .then((provinces) => {
      fetch(require("./popups.geojson")).then(res => res.json())
        .then((popups) => {
          processGeojsons({ provinces, popups })
        })
    });
}

let popupLocations = {};
let max = 0;
function incrementCase({ province, geojson }) {
  let features = geojson["features"].map(feature => {
    if (feature["properties"]["namefin"] == province) {
      feature["properties"]["count"] += 1;
    }
    if (feature["properties"]["province"] == province) {
      feature["properties"]["count"] += 1;
    }
    return feature;
  });
  return { crs: geojson.crs, features, name: geojson.name, type: geojson.type };
}

function fixColor({ geojson }) {
  let features = geojson["features"].map(feature => {
    let count = parseInt((feature["properties"]["count"]));
    let t = Math.cbrt((count / max)) * 255;
    feature["properties"]["color"] = `rgba(${t * 1}, 0, 0, 1)`;
    return feature;
  });
  return { crs: geojson.crs, features, name: geojson.name, type: geojson.type };
}

function processGeojsons({ provinces, popups }) {
  popupLocations = popups;
  const provincesFeatures = provinces["features"].map(province => {
    return { geometry: province["geometry"], properties: { ...province["properties"], count: 0, color: "rgb(255, 255, 255)" } };
  })
  let provincesGeojson = { crs: provinces.crs, features: provincesFeatures, name: provinces.name, type: provinces.type };
  const clusterFeatures = popups["features"].map(province => {
    return { geometry: province["geometry"], properties: { ...province["properties"], count: 0 } };
  })

  let clusterGeojson = { crs: provinces.crs/*just in case*/, features: clusterFeatures, name: "clusters", type: "FeatureCollection" };

  //fetch(require("./backup_1503.geojson"))
  fetch("https://w3qa5ydb4l.execute-api.eu-west-1.amazonaws.com/prod/finnishCoronaData/v2")
    .then(res => res.json())
    .then((res) => {
      let recoveredDistricts = {};
      res["recovered"].forEach(cCase => {
        if (cCase["healthCareDistrict"] === "HUS") cCase["healthCareDistrict"] = "Uusimaa";
        if (cCase["healthCareDistrict"] === "Vaasa") cCase["healthCareDistrict"] = "Pohjanmaa";
        if (cCase["healthCareDistrict"] === "Itä-Savo") cCase["healthCareDistrict"] = "Etelä-Savo";
        if (cCase["healthCareDistrict"] === "Länsi-Pohja") cCase["healthCareDistrict"] = "Lappi";
        if (cCase["healthCareDistrict"] in recoveredDistricts) recoveredDistricts[cCase["healthCareDistrict"]] += 1;
        else recoveredDistricts[cCase["healthCareDistrict"]] = 1;
      });
      let confirmed = res["confirmed"].map(cCase => {
        if (cCase["healthCareDistrict"] === "HUS") cCase["healthCareDistrict"] = "Uusimaa";
        if (cCase["healthCareDistrict"] === "Vaasa") cCase["healthCareDistrict"] = "Pohjanmaa";
        if (cCase["healthCareDistrict"] === "Itä-Savo") cCase["healthCareDistrict"] = "Etelä-Savo";
        if (cCase["healthCareDistrict"] === "Länsi-Pohja") cCase["healthCareDistrict"] = "Lappi";
        cCase["depth"] = -1;
        cCase["color"] = "rgba(255, 0, 0, 1)"
        return cCase;
      }).reverse().filter(cCase => {
        if (cCase["healthCareDistrict"] in recoveredDistricts) {
          recoveredDistricts[cCase["healthCareDistrict"]] -= 1;
          if (recoveredDistricts[cCase["healthCareDistrict"]] >= 0) return false;
        }
        return true;
      })
      let countUp = new CountUp('countup', confirmed.length, {separator: ''});
      countUp.start();
      confirmed.filter(f => f.healthCareDistrict !== null).forEach(cCase => {
        provincesGeojson = incrementCase({ geojson: provincesGeojson, province: cCase['healthCareDistrict'] })
        clusterGeojson = incrementCase({ geojson: clusterGeojson, province: cCase['healthCareDistrict'] })
      });
      provincesGeojson.features.forEach(feature => {
        max = Math.max(feature.properties.count, max);
      })
      fixColor({ geojson: provincesGeojson });
      map.addSource('provinces', {
        'type': 'geojson',
        'data': provincesGeojson
      });
      map.addSource('clusters', {
        'type': 'geojson',
        'data': clusterGeojson
      });
      map.addLayer({
        'id': 'provinces',
        'type': 'fill',
        'source': 'provinces',
        'layout': {},
        'paint': {
          'fill-opacity': 0.2,
          'fill-color': ['get', 'color']
        }
      });
      map.addLayer({
        id: 'cluster-count',
        type: 'symbol',
        source: 'clusters',
        filter: ['!=', 'count', 0],
        //maxzoom: 6.1,
        layout: {
          'text-field': '{count}',
          'text-ignore-placement': true,
          'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
          'text-size': {
            "stops": [
              [
                4,
                20
              ],
              [
                6,
                40
              ]
            ]
          }
        },
        "paint": {
          "text-color": "rgba(58, 58, 58, 1)",
          "text-halo-color": "rgba(255,255,255,0.7)",
          "text-halo-width": 1
        }
      });
    });
}
