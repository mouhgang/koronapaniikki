export function createGraph(params) {
  params["json"]= params["json"].filter(v => v["healthCareDistrict"] === params["province"]);
  let purePatients =
    params["json"].map(v => {
      let color = "#f00";
      if (v.infectionSourceCountry !== "FIN") color = "#ffd70f";
      else if (v.infectionSource === "unknown") color = "#db6e00";
      return {
        id: parseInt(v.id),
        label: v.id,
        color
      }
    });
  let patients = new vis.DataSet(
    purePatients
  );
  let pureEdges = params["json"].map(v => {
    if (v.infectionSource !== "unknown") {
      if (v.infectionSource === "related to earlier") {
      return {
        from: parseInt(v.id) - 1, // just a guess
        to: parseInt(v.id),
        arrows: {
          to: {
            enabled: true,
            type: "arrow"
          }
        }
      }
      }
      return {
        from: v.infectionSource,
        to: parseInt(v.id),
        arrows: {
          to: {
            enabled: true,
            type: "arrow"
          }
        }
      }
    }
  }).filter(v => v !== undefined);
  let spread = new vis.DataSet([
    ...pureEdges
  ])
  var container = document.getElementById(params["container"]);
  var data = {
    nodes: patients,
    edges: spread
  };
  var options = {interaction: {dragNodes: false, dragView: false, selectable: false, zoomView: false}};
  var network = new vis.Network(container, data, options);
}